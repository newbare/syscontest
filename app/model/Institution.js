Ext.define('SysContest.model.Institution',{
	extend  : 'Ext.data.Model',

	fields : [
			{name : 'idInstitution' , type : 'int'},
			{name : 'name', type : 'string'}
	]
});