Ext.define('SysContest.model.DisciplineSubject', {
	extend : 'Ext.data.Model',

	fields : [
	  {name : 'idDXS', type : 'int'},
			{name : 'idDiscipline', type : 'int'},
			{name : 'idSubject' , type : 'int'}
	]

});