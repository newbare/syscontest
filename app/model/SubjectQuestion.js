Ext.define('SysContest.model.SubjectQuestion', {
	extend : 'Ext.data.Model',

	fields : [
	  {name : 'idSXQ', type : 'int'},
			{name : 'idSubject', type : 'int'},
			{name : 'idQuestion' , type : 'int'}
	]

});