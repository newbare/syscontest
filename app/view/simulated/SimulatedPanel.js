Ext.define('SysContest.view.simulated.SimulatedPanel',{
		extend : 'Ext.panel.Panel',
		alias  : 'widget.simulatedpanel',
		title : 'QUESTÕES DE CONCURSO PÚBLICO',
		height: 400,
 	layout : {
					type : 'vbox',
					align: 'stretch'
		},
  autoScroll : true,
  autoShow : true,
  bodyStyle: 'padding:10px'
});